import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Logger,
  Param,
  ParseIntPipe,
  Post,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { CreateUserDto } from './user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  private readonly logger = new Logger(UserController.name);
  @Inject(UserService)
  private readonly service: UserService;

  @Get(':id')
  public async getUser(
    @Param('id', ParseIntPipe) id: number,
    @Res({ passthrough: true }) res: Response,
  ) {
    const user = await this.service.getUser(id);
    if (user !== null)
      return res
        .status(HttpStatus.OK)
        .json({ data: { status: 'true', user: user } });

    return res
      .status(HttpStatus.OK)
      .json({ data: { status: false, message: 'No User Found!' } });
  }

  @Post()
  public async createUser(@Body() body: CreateUserDto): Promise<User> {
    const userExist = await this.service.exists(body.email);
    this.logger.log(`Email Exists ${userExist}`);
    if (userExist) {
      throw new HttpException('Email is already taken', HttpStatus.BAD_REQUEST);
    }
    return this.service.createUser(body);
  }
}
