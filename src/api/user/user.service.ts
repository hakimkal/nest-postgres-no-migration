import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './user.dto';

@Injectable()
export class UserService {
  private readonly logger: Logger = new Logger(UserService.name);
  @InjectRepository(User)
  private readonly repository: Repository<User>;

  public getUser(id: number): Promise<User> {
    return this.repository.findOneBy({ id: id });
  }

  public exists(userEmail: string): Promise<boolean> {
    return this.repository.findBy({ email: userEmail }).then((r) => {
      return r.length > 0;
    });
  }

  public createUser(body: CreateUserDto): Promise<User> {
    const user: User = new User();
    this.logger.log(JSON.stringify(body));
    user.name = body.name;
    user.email = body.email;
    user.corporateEmail = body.corporateEmail;

    return this.repository.save(user);
  }
}
