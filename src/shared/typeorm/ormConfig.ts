import { DataSource } from 'typeorm';
import { TypeOrmConfigService } from './typeorm.service';

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: TypeOrmConfigService.DATABASE_HOST,
  port: parseInt(TypeOrmConfigService.DATABASE_PORT),
  database: 'nest-api-prod',
  username: TypeOrmConfigService.DATABASE_USERNAME,
  password: TypeOrmConfigService.DATABASE_PASSWORD,
  name: 'default',
  logging: 'all',
  synchronize: false,
  entities: ['dist/**/*.entity.{ts,js}'],
  migrations: ['dist/migrations/*.{ts,js}'],
  migrationsTableName: 'typeorm_migrations',
});

// AppDataSource.initialize()
//   .then(() => {
//     console.log('Data Source has been initialized!');
//   })
//   .catch((err) => {
//     console.error('Error during Data Source initialization', err);
//   });
