import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  @Inject(ConfigService)
  private readonly config: ConfigService;

  public static DATABASE_HOST: string;
  public static DATABASE_NAME: string;
  public static DATABASE_PASSWORD: string;
  public static DATABASE_USERNAME: string;
  public static DATABASE_PORT: string;

  public createTypeOrmOptions(): TypeOrmModuleOptions {
    TypeOrmConfigService.DATABASE_HOST =
      this.config.get<string>('DATABASE_HOST');
    TypeOrmConfigService.DATABASE_PORT =
      this.config.get<string>('DATABASE_PORT');
    TypeOrmConfigService.DATABASE_NAME =
      this.config.get<string>('DATABASE_NAME');
    TypeOrmConfigService.DATABASE_USERNAME =
      this.config.get<string>('DATABASE_USER');
    TypeOrmConfigService.DATABASE_PASSWORD =
      this.config.get<string>('DATABASE_PASSWORD');

    return {
      type: 'postgres',
      host: this.config.get<string>('DATABASE_HOST'),
      port: this.config.get<number>('DATABASE_PORT'),
      database: this.config.get<string>('DATABASE_NAME'),
      username: this.config.get<string>('DATABASE_USER'),
      password: this.config.get<string>('DATABASE_PASSWORD'),
      entities: ['dist/**/*.entity.{ts,js}'],
      migrations: ['dist/migrations/*.{ts,js}'],
      migrationsTableName: 'typeorm_migrations',
      logger: 'file',
      synchronize: false, // never use TRUE in production!
    };
  }
}
